;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((org-mode . ((org-roam-capture-templates . (("d" "default" plain "%?" :if-new
                                              (file+head "${slug}.org" "#+title: ${title}\n

#+author: %{Build Author}\n
#+setupfile:\n
#+hugo_base_dir:../\n
#+hugo_section: %^{Class||Warlock|Titan|Hunter}\n
#+hugo_bundle: ${slug}\n
#+hugo_draft: %^{Is it a draft|true|false}\n
#+export_file_name: index.md\n\n")
                                              :unnarrowed t)))))
 ("content-org/" . ((org-mode . ((eval org-hugo-auto-export-mode))))))
