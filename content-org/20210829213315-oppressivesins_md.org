:PROPERTIES:
:ID:       4e37ead5-ff95-4f6f-948c-91b98e46cdfc
:END:
#+title: Oppressive Sins
#+author: jhilker
#+setupfile:
#+hugo_base_dir: ../ 
#+hugo_draft: false
#+hugo_section: warlock/s12/
#+hugo_bundle: oppressive-sins
#+export_file_name: index.md


*PvE or PvP?:* PvE.

*Subclass:* Voidwalker, any branch.

* Gear 

- *Exotic Armor*: Nezerac's Sin.

** Weapons
- *Primary:* Witherhoard
- *Energy:* [[https://d2gunsmith.com/w/821154603?s=0,1087426260,0,0,2697220197,0][Gnawing Hunger]] (though really any of the column 1 perks work)
- *Heavy:* Any void weapon

** Ideal Mods
- Ashes to Assets
- Energy Converter
- Firepower
